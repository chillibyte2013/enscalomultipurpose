<?php
/**
Template Name: Page
 */

get_header();

$option = get_option('enscalomultipurpose_default_template', 'fixed-width');

if( $option == "left" ) {
    $left_grid = get_option( 'enscalomultipurpose_custom_left_grid', 4 );
} else if( $option == "right" ) {
    $right_grid = get_option( 'enscalomultipurpose_custom_right_grid', 4 );
}

if( $option == "fixed-width" ) { ?>

    <div class="container">

        <?php get_template_part( 'content', $option ); ?>

    </div>

<?php } else if( $option == "span" ) { ?>

    <div class="container-fluid row">

        <?php get_template_part( 'content', $option ); ?>

    </div>

<?php } else if( $option == "left" ) { ?>

    <div class="container-fluid row">

        <div class="<?php echo enscalomultipurpose_grid_class($left_grid); ?>">

            <?php get_template_part( 'content', $option ); ?>

        </div>

        <div class="<?php echo enscalomultipurpose_grid_class_remaining($left_grid); ?>">

            <?php get_template_part( 'content', 'span' ); ?>

        </div>

    </div>

<?php } else if( $option == "right" ) { ?>

    <div class="container-fluid row">

        <div class="<?php echo enscalomultipurpose_grid_class($right_grid); ?>">

            <?php get_template_part( 'content', $option ); ?>

        </div>

        <div class="<?php echo enscalomultipurpose_grid_class_remaining($right_grid); ?>">

            <?php get_template_part( 'content', 'span' ); ?>

        </div>

    </div>

<?php } ?>

<?php get_footer(); ?>