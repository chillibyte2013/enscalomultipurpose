<?php

require_once dirname(__FILE__) . '/inc/plugins/advanced-custom-fields/advanced-custom-fields.php';

add_action( 'after_setup_theme', 'enscalomultipurpose_setup' );

// hook into the init action and call custom_post_formats_taxonomies when it fires
add_filter( 'wp_terms_checklist_args', 'enscalomultipurpose_wp_terms_checklist_args' );

add_action( 'admin_init', array( 'enscalomultipurpose_admin_init' ) );

if(class_exists('Walker_Category_Checklist')) {
    class Walker_Format_Checklist extends Walker_Category_Checklist {

        public function walk( $elements, $max_depth )
        {
            $output = parent::walk($elements, $max_depth);

            return str_replace(['type="radio"', "type='radio'"], ['type="checkbox"', "type='checkbox'"], $output);
        }

    }
}

function enscalomultipurpose_setup() {

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 1568, 9999 );

    register_nav_menus(
        array(
            'menu-1' => __( 'Primary', 'enscalomultipurpose' ),
            'footer' => __( 'Footer Menu', 'enscalomultipurpose' )
        )
    );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support(
        'html5',
        array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        )
    );

    add_theme_support( 'custom-logo', array(
        'height'      => 300,
        'width'       => 400,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ) );

    add_theme_support( 'customize-selective-refresh-widgets' );

    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );

    // Add support for Block Styles.
    add_theme_support( 'wp-block-styles' );

    // Add support for editor styles.
    add_theme_support( 'editor-styles' );

    // Enqueue editor styles.
    add_editor_style( 'style-editor.css' );

    // Add support for responsive embedded content.
    add_theme_support( 'responsive-embeds' );

    // create custom post formats
    enscalomultipurpose_create_post_formats();

    // add post-formats to post_type 'page'
    add_post_type_support( 'post', 'post-formats' );
    add_post_type_support( 'page', 'post-formats' );
}

function enscalomultipurpose_create_post_formats() {

    if ( !term_exists( 'Fixed Width', 'post_format' ) ) {
        wp_insert_term('Fixed Width', 'post_format');
    }
    if ( !term_exists( 'Span', 'post_format' ) ) {
        wp_insert_term('Span', 'post_format');
    }
    if ( !term_exists( 'Left', 'post_format' ) ) {
        wp_insert_term('Left', 'post_format'); // left-grid scaler field
    }
    if ( !term_exists( 'Right', 'post_format' ) ) {
        wp_insert_term('Right', 'post_format'); // right-grid scaler field
    }

}

function enscalomultipurpose_wp_terms_checklist_args( $args ) {

    $args[ 'taxonomy' ] = 'post_format';
    $args[ 'checked_ontop' ] = false;

    $args[ 'walker' ] = 'Walker_Format_Checklist';

    return $args;
}

function enscalomultipurpose_grid_class( $grid_num ) {

    $grid_classes = ["col"];

    switch( $grid_num ) {

        case 2:
            array_push($grid_classes, "col-lg-{$grid_num} col-md-{$grid_num} col-sm-6 col-xs-6");
            break;
        case 3:
            array_push($grid_classes, "col-lg-{$grid_num} col-md-{$grid_num} col-sm-6 col-xs-6");
            break;
        case 4:
            array_push($grid_classes, "col-lg-{$grid_num} col-md-{$grid_num} col-sm-12 col-xs-12");
            break;
        case 5:
            array_push($grid_classes, "col-lg-{$grid_num} col-md-{$grid_num} col-sm-12 col-xs-12");
            break;
        case 6:
            array_push($grid_classes, "col-lg-{$grid_num} col-md-{$grid_num} col-sm-12 col-xs-12");
            break;
        default:
            array_push($grid_classes, "col-lg-12 col-md-12 col-sm-12 col-xs-12");
            break;

    }

    return implode(' ', $grid_classes);
}

function enscalomultipurpose_can_show_post_thumbnail() {
    return apply_filters( 'enscalomultipurpose_can_show_post_thumbnail', ! post_password_required() && ! is_attachment() && has_post_thumbnail() );
}

/**
 * Tokenize sentences
 * @param $sentence
 * @return array
 */
function enscalomultipurpose_sent_tokenize( $sentence, $punc = false ) {
    $punctuation = [';', ':', '.', '!', '?'];

    $punctuation = array_map(function($punc) {
        return "\{$punc}";
    }, $punctuation);

    $pattern = implode('|', $punctuation);

    return !$punc ? array_filter(preg_split( $pattern, $sentence )) : $punctuation;
}

/**
 * Admin interface changes
 */
function enscalomultipurpose_admin_init() {

    $option = get_option('enscalomultipurpose_layout_meta_boxes', array('post', 'page'));

    foreach($option as $post_type) {
        add_meta_box( 'layout-meta', __('Layout Meta', 'enscalomultipurpose'), array( 'enscalomultipurpose_meta_box' ), $post_type, 'normal' );
    }

    add_action( 'save_post', 'enscalo_multipurpose_save_post', 1, 2 );

}

function enscalomultipurpose_meta_box( $post ) {

    $type = $post->post_type;
    $layout_meta = get_post_meta( $post->ID, 'layout_meta_thumbnail', true );

    ?>

    <div class="clearfix">

        <label>Select a Thumbnail format
            <select name="emlp-layout-meta-thumbnail">
                <option value="thumbnail-full" <?php echo ( $layout_meta == "thumbnail-full" ) ? "selected='selected'" : ""; ?>>Default (Full)</option>
                <option value="thumbnail-centered" <?php echo ( $layout_meta == "thumbnail-centered" ) ? "selected='selected'" : ""; ?>>Centered</option>
                <option value="thumbnail-left" <?php echo ( $layout_meta == "thumbnail-left" ) ? "selected='selected'" : ""; ?>>Left</option>
                <option value="thumbnail-right" <?php echo ( $layout_meta == "thumbnail-right" ) ? "selected='selected'" : ""; ?>>Right</option>
            </select>
        </label>

    </div>

<?php
}

function enscalomultipurpose_save_post( $post_id, $post ) {

    $option = get_option('enscalomultipurpose_layout_meta_boxes', array('post', 'page'));

    foreach ($option as $post_type) {

        if ( $post->post_type == $post_type ) {
            $meta = get_post_meta( $post_id, 'layout_meta_thumbnail', true );
            $value = filter_var('emlp-layout-meta-thumbnail', INPUT_POST);
            if (empty($meta)) {
                add_post_meta( $post_id, 'layout_meta_thumbnail', $value );
            } else {
                update_post_meta( $post_id, 'layout_meta_thumbnail', $value );
            }
        }

    }

}

function enscalomultipurpose_get_content() {
    ob_start();
    the_content();
    return ob_get_clean();
}

function render_template($template_part, $name, $variables) {
    extract($variables);
    get_template_part($template_part, $name);
}