<section class="entry-field-content field-<?php echo $field_object['type'] . '-' . $field_object['name'] . '-' . $field_object['key']; ?>">
    <div class="clearfix">
        <?php the_field($name); ?>
    </div>
</section>