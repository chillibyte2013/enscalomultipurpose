<?php
/**
 * The default template for displaying content.
 */
$fields = get_fields(get_the_ID());
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( ! enscalomultipurpose_can_show_post_thumbnail() ) { ?>
        <div class="entry-content">
            <?php the_content(); ?>
        </div>
        <div class="entry-fields">
            <?php foreach( $fields as $name => $value ) {
                $field_object = get_field_object($name);
                render_template('template-parts/field/field', $field_object['type'], array(
                    'field_object' => $field_object,
                    'name' => $name
                ));
            } ?>
        </div>
	<?php } else {
        $layout_meta = get_post_meta( $post->ID, 'layout_meta_thumbnail', true );
        if ( $layout_meta == "thumbnail-centered" ) { ?>
        <div class="entry-content">
        <?php
            the_post_thumbnail('medium', ['class' => 'rounded mx-auto d-block']);
            the_content();
        ?>
        </div>
        <?php } else if( $layout_meta == "thumbnail-left" || $layout_meta == "thumbnail-right" ) { ?>
        <div class="entry-content">
            <div class="text-left float-<?php echo str_replace('thumbnail-', '', $layout_meta); ?>">
            <?php the_post_thumbnail('medium', ['class' => 'rounded float-' . str_replace('thumbnail-', '', $layout_meta) ]); ?>
            </div>
            <?php the_content(); ?>
        </div>
        <?php } else { ?>
        <div class="entry-content">
            <div class="text-left clearfix">
                <?php the_post_thumbnail('full', ['class' => 'img-fluid']); ?>
            </div>
            <?php the_content(); ?>
        </div>
        <?php }
    } ?>
</article><!-- #post-<?php the_ID(); ?> -->
